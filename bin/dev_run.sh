#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${ORIGINAL_SCRIPT_PATH}/libs/debug"
source "${ORIGINAL_SCRIPT_PATH}/libs/functions"
source "${ORIGINAL_SCRIPT_PATH}/libs/java_functions"
source "${ORIGINAL_SCRIPT_PATH}/libs/maven_functions"

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd .. || exit 1

print_random_banner

print_execution_environment_information

set_maven_memory
print_maven_execution_environment

set_java_memory
print_java_execution_environment

echo

# Initialize
############

#./mvnw clean package -U -DskipTests -T 1C

# Run
############

spring_profiles_active="dev"
export spring_profiles_active

while true; do

    ./mvnw spring-boot:run -Dspring-boot.run.workingDirectory=working-directory

	if [ $? -eq 0 ]; then

		echo
        echo -e "${GREEN} Control+C twice to stop and exit"
        echo
        echo -e "${BOLD_BLUE}           _                 _ _             "
        echo -e "          | |               | (_)            "
        echo -e "  _ __ ___| | ___   __ _  __| |_ _ __   __ _ "
        echo -e " | '__/ _ \ |/ _ \ / _\` |/ _\` | | '_ \ / _\` |"
        echo -e " | | |  __/ | (_) | (_| | (_| | | | | | (_| |"
        echo -e " |_|  \___|_|\___/ \__,_|\__,_|_|_| |_|\__, |"
        echo -e "                                        __/ |"
        echo -e "                                       |___/ ${RESET}"

	fi

done

exit 0
