package project_name.api_rest.mapper;

import project_name.api_rest.model.ArticuloDTO;
import project_name.database.model.ArticuloMO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ArticulosMAPPER {

    ArticulosMAPPER INSTANCE = Mappers.getMapper(ArticulosMAPPER.class);

    @Mapping(source = "eanMerca", target = "ean")
    ArticuloDTO aDTO(ArticuloMO articulo);

}
