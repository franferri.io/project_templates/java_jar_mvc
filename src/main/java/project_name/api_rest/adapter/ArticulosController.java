package project_name.api_rest.adapter;

import project_name.api_rest.mapper.ArticulosMAPPER;
import project_name.api_rest.model.ArticuloDTO;
import project_name.database.model.ArticuloMO;
import project_name.database.repositories.ArticulosREPOSITORY;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class ArticulosController {

    private final ArticulosREPOSITORY database;
    private final ArticulosMAPPER mapper = ArticulosMAPPER.INSTANCE;

    @GetMapping("/articulos/{name}")
    public ArticuloDTO getArticulo(@PathVariable String name) {

        // MODEL VIEW CONTROLLER
        System.out.println("MVC");

        // VIEW
        // El view ha recibido la llamada REST
        // En este ejemplo el VIEW es el API REST
        System.out.println("Llamada recibida en el VIEW (API REST) en /v1/articulos/ con valor: " + name);

        // A modo de ejemplo creamos en la base de datos un artículo llamado "name"
        System.out.println("El CONTROLLER crea un Artículo llamado " + name + " de ejemplo en la base de datos");
        ArticuloMO clinex = new ArticuloMO();
        clinex.setId(1234L);
        clinex.setDescription("Paquete de clinex");
        clinex.setName(name);
        clinex.setEanMerca("00012341234");
        database.save(clinex);

        // CONTROLLER
        // Este controller llama a la base de datos (modelo)
        System.out.println("El CONTROLLER llama al MODELO para recibir los datos de: " + name);
        ArticuloMO articulo = database.getArticulo(name);

        // MODEL
        //Este controller devuelve el dato recibido del modelo relacional
        System.out.println("El CONTROLLER devuelve los datos recibidos al VIEW");
        return mapper.aDTO(articulo);

    }

}



