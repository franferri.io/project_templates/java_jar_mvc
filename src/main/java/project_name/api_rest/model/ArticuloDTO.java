package project_name.api_rest.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ArticuloDTO {

    private final String name;
    private final String ean;

}
