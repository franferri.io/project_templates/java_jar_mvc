package project_name.database.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@Entity
@Table(name = "ARTICULOS")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ArticuloMO {

    @Id
    private Long id;

    @Column(name = "nombre")
    private String name;

    @Column(name = "eanmerca")
    private String eanMerca;

    @Column(name = "descripcion")
    private String description;

}
